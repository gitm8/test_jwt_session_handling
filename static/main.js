const tokenDisplay = document.getElementById('tokenDisplay');
const logoutButton = document.getElementById('logoutButton');
const loginButton = document.getElementById('loginButton');

const parseJwt = token => JSON.parse(window.atob(token.split('.')[1]).replace(/-/g, '+').replace(/_/g, '/'))
let savedToken;

function checkToken() {
  const token = localStorage.getItem('token')

  if (token) {
    const { exp } = parseJwt(token)
    const tokenExpired = exp < Date.now();

    logoutButton.disabled = false
    loginButton.disabled = true
    tokenDisplay.innerHTML = JSON.stringify(parseJwt(token), null, 2)
  } else {
    tokenDisplay.innerHTML = ''

    logoutButton.disabled = true
    loginButton.disabled = false
  }
}

function login() {
  const user = document.getElementById('username').value
  const password = document.getElementById('password').value

  fetch('http://localhost:3001/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user,
      password
    })
  })
    .then(res => res.json())
    .then(({ token }) => {
      savedToken = token
      localStorage.setItem('token', token)
      checkToken()
      checkAuth()
    })
    .catch(console.error)
}

function logout() {
  const token = localStorage.getItem('token')

  localStorage.removeItem('token')

  !token ? checkToken() : fetch('http://localhost:3001/logout', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`
    },
    body: JSON.stringify({ token })
  })
    .then(res => {
      checkToken()
    })
}

function checkAuth() {
  const interval = setInterval(() => {
    fetch('http://localhost:3001/auth', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token') || savedToken}`
      }
    })
      .then(r => {
        if (r.status === 401) {
          // clearInterval(interval)
          return logout()
        }
        return r.json()
      })
      .catch(err => {
        console.log('ping', err.message)
      })
  }, 10000)
}
