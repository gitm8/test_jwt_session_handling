const path = require('path');
const jwt = require('jsonwebtoken');
const express = require('express');
const levelup = require('level');
const ttl = require('level-ttl')
const debug = require('debug')('test')

const tokenBlackListDb = ttl(levelup('.tokenBlackListDb'))

const SESSION_PASS = 'sessionPassword';
const port = 3001
const server = express();

function authenticate(req, res, next) {
  const token = req.headers.authorization.split(' ')[1];

  jwt.verify(token, SESSION_PASS, (err, verifiedToken) => {
    if (err) {
      err.statusCode = 401
      return next(err)
    }

    const { user, exp } = verifiedToken;

    tokenBlackListDb.get(token, (err, retrievedToken) => {
      if (err && err.name === 'NotFoundError') {
        res.locals.user = user
        return next()
      } else if (err) {
        return next(err)
      }

      const blackListedError = new Error('jwt expired (bl)')

      blackListedError.statusCode = 401
      next(blackListedError)
    })
  })
}

function requestLogger(req, res, next) {
  next()
  const sendJson = res.json;

  res.json = (...args) => {
    res.locals.body = args[0];
    sendJson.apply(res, args)
  }
  const then = Date.now()

  res.on('finish', () => {
    debug(`req ${req.method} ${res.statusCode} ${Date.now() - then}ms ${req.path} ${res.statusCode > 299 ? '"' + res.locals.body.message + '"' : ''}`)
  })
}

function loginHandler(req, res, next) {
  const { user, password } = req.body;
  const exp = Math.floor(Date.now() / 1000) + (60);
  const token = jwt.sign({ user, exp }, SESSION_PASS);

  res.json({ token })
}

function logoutHandler(req, res, next) {
  const token = req.headers.authorization.split(' ')[1];

  if(!token) {
    return res.json({})
  }

  jwt.verify(req.body.token, SESSION_PASS, (err, verifiedToken) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        return res.json({})
      }
      return next(err)
    }

    const { user, exp } = verifiedToken
    const ttl = exp - Date.now()

    tokenBlackListDb.put(token, user, { ttl }, function (err) {
      if(err) return next(err)
      res.json({})
    })

  })
}

server.disable('x-powered-by')

/* general Middleware */
server.use(express.json());
server.use(requestLogger)
server.use(express.static('static'));

/* Route handler */
server.post('/login', loginHandler);
server.post('/logout', logoutHandler)
server.get('/auth', authenticate, (req, res, next) => res.json({ msg: 'authenticated' }))

/* Error handler */
server.use((err, req, res, next) => {
  if(!err.statusCode) {
    err.statusCode = 500
  }

  const { message } = err
  res.status(err.statusCode)
  res.json({
    message: err.message,
    stack: err.stack
  })
})

server.listen(port, () => debug(`Listening on ${port}`))
